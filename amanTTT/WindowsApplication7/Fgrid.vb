﻿Imports WindowsApplication7.Setting

Public Class Fgrid
    Dim pilihan As Integer = 3
    Dim mode As String = "classic"


    Private Sub btOk_Click(sender As Object, e As EventArgs) Handles btOk.Click
        If rb5.Checked Then
            ukuran = 5
        ElseIf rb9.Checked Then
            ukuran = 9
        Else
            ukuran = 3
        End If

        If rbPoin.Checked Then
            mode = rbPoin.Text
            Me.Hide()
            poin.Show()
        Else
            mode = rbClassic.Text
            Me.Hide()
            Classic.ShowDialog()
        End If


    End Sub

    Private Sub Fgrid_Load(sender As Object, e As EventArgs) Handles MyBase.Shown
        If AI_On() Then
            rbPoin.Hide()
            Label1.Hide()
            rbClassic.Hide()
        End If
    End Sub

    Private Sub rb3_CheckedChanged(sender As Object, e As EventArgs) Handles rb3.CheckedChanged
        PlaySoundEffectOnce()
    End Sub

    Private Sub rb5_CheckedChanged(sender As Object, e As EventArgs) Handles rb5.CheckedChanged
        PlaySoundEffectOnce()
    End Sub

    Private Sub rb9_CheckedChanged(sender As Object, e As EventArgs) Handles rb9.CheckedChanged
        PlaySoundEffectOnce()
    End Sub

    Private Sub rbClassic_CheckedChanged(sender As Object, e As EventArgs) Handles rbClassic.CheckedChanged
        PlaySoundEffectOnce()
    End Sub

    Private Sub rbPoin_CheckedChanged(sender As Object, e As EventArgs) Handles rbPoin.CheckedChanged
        PlaySoundEffectOnce()
    End Sub

    Private Sub btCancel_Click(sender As Object, e As EventArgs) Handles btCancel.Click
        PlaySoundEffectOnce()
        Me.Close()
        Fhome.Show()
    End Sub
End Class