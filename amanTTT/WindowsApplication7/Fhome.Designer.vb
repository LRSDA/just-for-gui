﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Fhome
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btSettings = New System.Windows.Forms.Button()
        Me.btMultiPlayer = New System.Windows.Forms.Button()
        Me.btSinglePlayer = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btSettings
        '
        Me.btSettings.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btSettings.Font = New System.Drawing.Font("Candara", 48.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSettings.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btSettings.Location = New System.Drawing.Point(66, 550)
        Me.btSettings.Name = "btSettings"
        Me.btSettings.Size = New System.Drawing.Size(461, 92)
        Me.btSettings.TabIndex = 10
        Me.btSettings.Text = "Settings"
        Me.btSettings.UseVisualStyleBackColor = False
        '
        'btMultiPlayer
        '
        Me.btMultiPlayer.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btMultiPlayer.Font = New System.Drawing.Font("Candara", 48.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMultiPlayer.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btMultiPlayer.Location = New System.Drawing.Point(66, 439)
        Me.btMultiPlayer.Name = "btMultiPlayer"
        Me.btMultiPlayer.Size = New System.Drawing.Size(461, 92)
        Me.btMultiPlayer.TabIndex = 9
        Me.btMultiPlayer.Text = "Multi Player"
        Me.btMultiPlayer.UseVisualStyleBackColor = False
        '
        'btSinglePlayer
        '
        Me.btSinglePlayer.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btSinglePlayer.Font = New System.Drawing.Font("Candara", 48.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btSinglePlayer.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btSinglePlayer.Location = New System.Drawing.Point(66, 330)
        Me.btSinglePlayer.Name = "btSinglePlayer"
        Me.btSinglePlayer.Size = New System.Drawing.Size(461, 92)
        Me.btSinglePlayer.TabIndex = 8
        Me.btSinglePlayer.Text = "Single Player"
        Me.btSinglePlayer.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button1.Image = Global.WindowsApplication7.My.Resources.Resources.logo
        Me.Button1.Location = New System.Drawing.Point(152, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(282, 279)
        Me.Button1.TabIndex = 0
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Fhome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(580, 685)
        Me.Controls.Add(Me.btSettings)
        Me.Controls.Add(Me.btMultiPlayer)
        Me.Controls.Add(Me.btSinglePlayer)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Fhome"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "home"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents btSettings As Button
    Friend WithEvents btMultiPlayer As Button
    Friend WithEvents btSinglePlayer As Button
End Class
