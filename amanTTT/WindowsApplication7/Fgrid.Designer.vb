﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Fgrid
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SettingsLabel = New System.Windows.Forms.Label()
        Me.DifficultyLabel = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btOk = New System.Windows.Forms.Button()
        Me.rb3 = New System.Windows.Forms.RadioButton()
        Me.rb5 = New System.Windows.Forms.RadioButton()
        Me.rb9 = New System.Windows.Forms.RadioButton()
        Me.rbPoin = New System.Windows.Forms.RadioButton()
        Me.rbClassic = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btCancel = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SettingsLabel
        '
        Me.SettingsLabel.AutoSize = True
        Me.SettingsLabel.BackColor = System.Drawing.Color.Transparent
        Me.SettingsLabel.Font = New System.Drawing.Font("Candara", 72.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SettingsLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SettingsLabel.Location = New System.Drawing.Point(8, 9)
        Me.SettingsLabel.Name = "SettingsLabel"
        Me.SettingsLabel.Size = New System.Drawing.Size(563, 117)
        Me.SettingsLabel.TabIndex = 1
        Me.SettingsLabel.Text = "Grid Settings"
        '
        'DifficultyLabel
        '
        Me.DifficultyLabel.AutoSize = True
        Me.DifficultyLabel.BackColor = System.Drawing.Color.Transparent
        Me.DifficultyLabel.Font = New System.Drawing.Font("Candara", 36.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DifficultyLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DifficultyLabel.Location = New System.Drawing.Point(191, 165)
        Me.DifficultyLabel.Name = "DifficultyLabel"
        Me.DifficultyLabel.Size = New System.Drawing.Size(236, 59)
        Me.DifficultyLabel.TabIndex = 5
        Me.DifficultyLabel.Text = "Dimension"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Candara", 36.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(220, 359)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(139, 59)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Mode"
        '
        'btOk
        '
        Me.btOk.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btOk.Font = New System.Drawing.Font("Candara", 38.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.btOk.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btOk.Location = New System.Drawing.Point(308, 550)
        Me.btOk.Name = "btOk"
        Me.btOk.Size = New System.Drawing.Size(178, 71)
        Me.btOk.TabIndex = 16
        Me.btOk.Text = "Play"
        Me.btOk.UseVisualStyleBackColor = False
        '
        'rb3
        '
        Me.rb3.AutoSize = True
        Me.rb3.Checked = True
        Me.rb3.Font = New System.Drawing.Font("Candara", 28.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.rb3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rb3.Location = New System.Drawing.Point(38, 19)
        Me.rb3.Name = "rb3"
        Me.rb3.Size = New System.Drawing.Size(112, 50)
        Me.rb3.TabIndex = 17
        Me.rb3.TabStop = True
        Me.rb3.Text = "3 X 3"
        Me.rb3.UseVisualStyleBackColor = True
        '
        'rb5
        '
        Me.rb5.AutoSize = True
        Me.rb5.Font = New System.Drawing.Font("Candara", 28.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.rb5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rb5.Location = New System.Drawing.Point(207, 19)
        Me.rb5.Name = "rb5"
        Me.rb5.Size = New System.Drawing.Size(112, 50)
        Me.rb5.TabIndex = 18
        Me.rb5.Text = "5 X 5"
        Me.rb5.UseVisualStyleBackColor = True
        '
        'rb9
        '
        Me.rb9.AutoSize = True
        Me.rb9.Font = New System.Drawing.Font("Candara", 28.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.rb9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rb9.Location = New System.Drawing.Point(372, 19)
        Me.rb9.Name = "rb9"
        Me.rb9.Size = New System.Drawing.Size(116, 50)
        Me.rb9.TabIndex = 19
        Me.rb9.Text = "9 X 9"
        Me.rb9.UseVisualStyleBackColor = True
        '
        'rbPoin
        '
        Me.rbPoin.AutoSize = True
        Me.rbPoin.Font = New System.Drawing.Font("Candara", 28.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.rbPoin.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbPoin.Location = New System.Drawing.Point(224, 19)
        Me.rbPoin.Name = "rbPoin"
        Me.rbPoin.Size = New System.Drawing.Size(108, 50)
        Me.rbPoin.TabIndex = 21
        Me.rbPoin.Text = "poin"
        Me.rbPoin.UseVisualStyleBackColor = True
        '
        'rbClassic
        '
        Me.rbClassic.AutoSize = True
        Me.rbClassic.Checked = True
        Me.rbClassic.Font = New System.Drawing.Font("Candara", 28.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.rbClassic.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rbClassic.Location = New System.Drawing.Point(25, 19)
        Me.rbClassic.Name = "rbClassic"
        Me.rbClassic.Size = New System.Drawing.Size(138, 50)
        Me.rbClassic.TabIndex = 20
        Me.rbClassic.TabStop = True
        Me.rbClassic.Text = "classic"
        Me.rbClassic.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rb3)
        Me.GroupBox1.Controls.Add(Me.rb5)
        Me.GroupBox1.Controls.Add(Me.rb9)
        Me.GroupBox1.Location = New System.Drawing.Point(34, 217)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(508, 100)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbPoin)
        Me.GroupBox2.Controls.Add(Me.rbClassic)
        Me.GroupBox2.Location = New System.Drawing.Point(95, 421)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(377, 81)
        Me.GroupBox2.TabIndex = 23
        Me.GroupBox2.TabStop = False
        '
        'btCancel
        '
        Me.btCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btCancel.Font = New System.Drawing.Font("Candara", 38.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.btCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btCancel.Location = New System.Drawing.Point(78, 550)
        Me.btCancel.Name = "btCancel"
        Me.btCancel.Size = New System.Drawing.Size(180, 71)
        Me.btCancel.TabIndex = 24
        Me.btCancel.Text = "Cancel"
        Me.btCancel.UseVisualStyleBackColor = False
        '
        'Fgrid
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(580, 685)
        Me.Controls.Add(Me.btCancel)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btOk)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DifficultyLabel)
        Me.Controls.Add(Me.SettingsLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Fgrid"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Fgrid"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents SettingsLabel As Label
    Friend WithEvents DifficultyLabel As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btOk As Button
    Friend WithEvents rb3 As RadioButton
    Friend WithEvents rb5 As RadioButton
    Friend WithEvents rb9 As RadioButton
    Friend WithEvents rbPoin As RadioButton
    Friend WithEvents rbClassic As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btCancel As Button
End Class
