﻿Public Class Chance
    Public _step As Coordinate
    Public _point As Integer = 0
    Dim _situation(,) As Integer
    Dim _max_point As Integer = 0
    Dim _6th_sense(,) As Integer
    Dim _phase As Integer

    Sub New(ByVal s As Coordinate, ByVal situation(,) As Integer, ByVal based(,) As Integer, ByVal p As Integer)

        _step = s

        For i = 0 To ukuran - 1
            For j = 0 To ukuran - 1
                _6th_sense(i, j) = based(i, j)
                _situation(i, j) = situation(i, j)
            Next
        Next
    End Sub

    Function checkCoordinate(ByVal enemyMove As Coordinate) As Boolean
        If _situation(enemyMove.Y, enemyMove.X) = 0 Then
            Return False
        End If
        Return True
    End Function

    Overridable Sub Learn(ByVal enemyMove As Coordinate)
        Dim bufferMax As Integer = 0

        _6th_sense(enemyMove.Y, enemyMove.X) = 0


        'Vertical
        For i = 0 To ukuran - 1
            If _6th_sense(i, enemyMove.X) = 0 AndAlso checkCoordinate(New Coordinate(enemyMove.X, i)) Then
                Continue For
            End If
            Debug.WriteLine(i & "V")
            updateHMVertical(New Coordinate(enemyMove.X, i))
        Next


        'Horizontal
        For i = 0 To ukuran - 1
            If _6th_sense(enemyMove.Y, i) = 0 AndAlso checkCoordinate(New Coordinate(i, enemyMove.Y)) Then
                Continue For
            End If
            Debug.WriteLine(i & "H")
            updateHMHorizontal(New Coordinate(i, enemyMove.Y))
        Next

        'Diagonal left
        If (enemyMove.isDL()) Then
            For i = 0 To ukuran - 1
                If _6th_sense(i, i) = 0 AndAlso checkCoordinate(New Coordinate(i, i)) Then
                    Continue For
                End If

                updateHMDiagonalLtoR(New Coordinate(i, i))
            Next
        End If
        Debug.WriteLine(enemyMove.isDL().ToString)

        'Diagonal right
        If enemyMove.isDR(ukuran) Then
            Dim j As Integer = (ukuran - 1)
            For i = 0 To ukuran - 1
                If _6th_sense(j, i) = 0 AndAlso checkCoordinate(New Coordinate(i, j)) Then
                    j -= 1
                    Continue For
                End If
                updateHMDiagonalRtoL(New Coordinate(i, j))
                j -= 1
            Next
        End If
        Debug.WriteLine(enemyMove.isDR(ukuran).ToString)

        For i = 0 To ukuran - 1
            For j = 0 To ukuran - 1
                If _6th_sense(i, j) > bufferMax Then

                    bufferMax = _6th_sense(i, j)
                End If
            Next
        Next

        _max_point = bufferMax

        For i = 0 To ukuran - 1
            For j = 0 To ukuran - 1
                Debug.Write("| " & _6th_sense(i, j) & "|")
            Next
            Debug.WriteLine("")
        Next

        Debug.WriteLine("")

        For i = 0 To ukuran - 1
            For j = 0 To ukuran - 1
                Debug.Write("| " & _situation(i, j) & "|")
            Next
            Debug.WriteLine("")
        Next

    End Sub

    Overridable Sub updateHMHorizontal(ByVal target As Coordinate)
        Dim humanCounter As Integer = 0
        Dim aiCounter As Integer = 0
        For i As Integer = 0 To ukuran - 1
            If _situation(target.Y, i) = 1 Then
                humanCounter += 1
            ElseIf _situation(target.Y, i) = -1 Then
                aiCounter += 1
            End If
        Next

        If humanCounter = ukuran - 1 Then
            _6th_sense(target.Y, target.X) += 3
        ElseIf aiCounter = ukuran - 1 Then
            _6th_sense(target.Y, target.X) += 10
        ElseIf humanCounter = 1 AndAlso aiCounter = 0 Then
            _6th_sense(target.Y, target.X) += 1
        ElseIf aiCounter = humanCounter And aiCounter = 1 Then
            _6th_sense(target.Y, target.X) -= 2
        End If
    End Sub

    Overridable Sub updateHMVertical(ByVal target As Coordinate)
        Dim humanCounter As Integer = 0
        Dim aiCounter As Integer = 0
        For I As Integer = 0 To ukuran - 1
            If _situation(I, target.X) = 1 Then
                humanCounter += 1
            ElseIf _situation(I, target.X) = -1 Then
                aiCounter += 1
            End If
        Next

        If humanCounter = ukuran - 1 Then
            _6th_sense(target.Y, target.X) += 3
        ElseIf aiCounter = ukuran - 1 Then
            _6th_sense(target.Y, target.X) += 10
        ElseIf humanCounter = 1 AndAlso aiCounter = 0 Then
            _6th_sense(target.Y, target.X) += 1
        ElseIf aiCounter = humanCounter And aiCounter = 1 Then
            _6th_sense(target.Y, target.X) -= 2
        End If

    End Sub

    Overridable Sub updateHMDiagonalLtoR(ByVal target As Coordinate)
        Dim humanCounter As Integer = 0
        Dim aiCounter As Integer = 0
        For I As Integer = 0 To ukuran - 1
            If _situation(I, I) = 1 Then
                humanCounter += 1
            ElseIf _situation(I, I) = -1 Then
                aiCounter += 1
            End If
        Next

        If humanCounter = ukuran - 1 Then
            _6th_sense(target.Y, target.X) += 3
        ElseIf aiCounter = ukuran - 1 Then
            _6th_sense(target.Y, target.X) += 10
        ElseIf humanCounter = 1 AndAlso aiCounter = 0 Then
            _6th_sense(target.Y, target.X) += 1
        ElseIf aiCounter = humanCounter And aiCounter = 1 Then
            _6th_sense(target.Y, target.X) -= 2
        End If

    End Sub

    Overridable Sub updateHMDiagonalRtoL(ByVal target As Coordinate)
        Dim humanCounter As Integer = 0
        Dim aiCounter As Integer = 0
        Dim J As Integer = ukuran - 1
        For I As Integer = 0 To ukuran - 1
            If _situation(J, I) = 1 Then
                humanCounter += 1
            ElseIf _situation(J, I) = -1 Then
                aiCounter += 1
            End If
            J -= 1
        Next

        If humanCounter = ukuran - 1 Then
            _6th_sense(target.Y, target.X) += 3
        ElseIf aiCounter = ukuran - 1 Then
            _6th_sense(target.Y, target.X) += 10
        ElseIf humanCounter = 1 AndAlso aiCounter = 0 Then
            _6th_sense(target.Y, target.X) += 1
        ElseIf aiCounter = humanCounter And aiCounter = 1 Then
            _6th_sense(target.Y, target.X) -= 2
        End If

    End Sub

    Overridable Function choose() As Coordinate
        If _max_point = 0 Then
            Return check_situation()
        Else
            For i = 0 To ukuran - 1
                For j = 0 To ukuran - 1
                    If _6th_sense(i, j) = _max_point Then
                        Return New Coordinate(j, i)
                    End If
                Next
            Next
        End If
        Return New Coordinate(ukuran - 1, ukuran - 1)
    End Function

    Function check_situation() As Coordinate
        For i = 0 To ukuran - 1
            For j = 0 To ukuran - 1
                If _situation(i, j) = 0 Then
                    Return New Coordinate(j, i)
                End If
            Next
        Next
        Return New Coordinate(ukuran - 1, ukuran - 1)
    End Function

    Sub predict()
        Dim bBuff As Boolean
        Dim choice As Coordinate = _step
        Dim turn As Integer = 1

        bBuff = _situation(choice.Y, choose.X) = -1
        Learn(choice)

        turn = 0
        choice = Me.choose()
        bBuff = _situation(choice.Y, choose.X) = -1
        Learn(choice)

        If Not checkCondition() = 0 AndAlso Not _phase >= ukuran ^ 2 Then
            _point = 10
        End If

        turn = 1
        choice = choose()
        bBuff = _situation(choice.Y, choose.X) = -1

        If Not checkCondition() = 0 AndAlso Not _phase >= ukuran ^ 2 Then
            _point = 0
        Else
            _point = 3
        End If


    End Sub

    Private Function checkCondition()
        Dim hitungD1 As Integer
        Dim hitungD2 As Integer
        Dim varD As Integer = ukuran - 1
        Dim draw As Boolean = True

        Debug.WriteLine("phase : " & _phase)

        For i As Integer = 0 To ukuran - 1
            Dim hitung As Integer = 0


            For x As Integer = 0 To ukuran - 1
                hitung += _situation(i, x)
            Next

            'cek hasil hitungan
            If hitung = -1 * ukuran Then
                winner = -1
                draw = False

            ElseIf hitung = ukuran Then
                winner = 1
                draw = False
            End If

            hitung = 0
            For x As Integer = 0 To ukuran - 1
                hitung += _situation(x, i)
            Next

            'cek hasil hitungan
            If hitung = -1 * ukuran Then
                winner = -1

                draw = False
            ElseIf hitung = ukuran Then
                winner = 1
                draw = False
            End If

            hitungD1 += _situation(i, i)
            hitungD2 += _situation(i, varD)
            varD -= 1
        Next

        'cek diagonal
        If hitungD1 = -1 * ukuran Or hitungD2 = -1 * ukuran Then
            winner = -1
            draw = False
        ElseIf hitungD1 = ukuran Or hitungD2 = ukuran Then
            winner = 1
            draw = False
        End If

        'cek Seri(turn habis)
        If (_phase >= ukuran * ukuran) And draw = True Then
            winner = 0
        End If

        Return winner

    End Function
End Class
