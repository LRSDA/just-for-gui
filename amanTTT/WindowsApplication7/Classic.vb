﻿Imports WindowsApplication7.Setting

Public Class Classic
    Dim phase As Integer = 0
    Dim petak(,) As Button
    Dim value(,) As Integer
    Dim finish As Boolean = False
    Dim oppenent As ArtificialIntelligence

    Private Sub F3x3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblPlayer1.Text = player1name
        lblPlayer2.Text = player2name

        Debug.WriteLine(player1name)
        Me.MaximizeBox = False

        'mengatur ukuran array
        ReDim value(ukuran - 1, ukuran - 1)
        ReDim petak(ukuran - 1, ukuran - 1)

        createField()
        resetBoard()
    End Sub

    Private Sub createField()
        Dim leftMargin As Integer = 20
        Dim sizePetak As Integer = 75
        phase = 0

        Debug.WriteLine("" & ukuran & "xxx ")

        Select Case ukuran
            Case -1
                MessageBox.Show("Pilih salah satu ukuran papan")
                kosongi()
                Exit Sub
            Case 3
                sizePetak = 165
            Case 5
                sizePetak = 96
            Case 9
                sizePetak = 50
                leftMargin = 25
        End Select

        'cek ukuran apa yang dipilih dan berapa jumlah menang yang dibutuhkan

        'setting ukuran form dan panel
        Me.Size = New Size(Me.Width, (18 + (sizePetak + 6) * ukuran) + 16)
        'Panel1.Size = New Size((sizePetak * ukuran) + (6 * (ukuran + 1)), (sizePetak * ukuran) + (6 * (ukuran + 1)))



        'generate petak
        For i As Integer = 0 To ukuran - 1
            For j As Integer = 0 To ukuran - 1
                petak(i, j) = New Button()
                Panel1.Controls.Add(petak(i, j))
                'edit tampilan button disini
                petak(i, j).Width = sizePetak
                petak(i, j).Font = New Font(Me.Font.FontFamily, Convert.ToSingle(Convert.ToDouble(sizePetak) / 2), FontStyle.Bold)
                petak(i, j).Height = sizePetak
                petak(i, j).Location = New Point(leftMargin + j * (sizePetak + 6), 6 + i * (sizePetak + 6))
                petak(i, j).Name = "petak" & CStr(i) & CStr(j)
                petak(i, j).Visible = True
                Debug.WriteLine(sizePetak)

                AddHandler petak(i, j).Click, AddressOf Me.btnPetak_Click
            Next
        Next
    End Sub

    Private Sub btnPetak_Click(sender As Object, e As EventArgs)
        PlaySoundEffectOnce()
        'event button di klik
        Dim myButton = DirectCast(sender, Button)
        Dim newPlace As Coordinate = New Coordinate(CInt(myButton.Name.Substring(6, 1)), CInt(myButton.Name.Substring(5, 1)))


        If phase Mod 2 = 0 Then
            value(newPlace.Y, newPlace.X) = 1
            myButton.BackgroundImage = System.Drawing.Image.FromFile(imagePathPlayer1 & player1icon & ".png")
            myButton.BackgroundImageLayout = ImageLayout.Stretch

            'myButton.Text = "X"
        Else
            value(newPlace.Y, newPlace.X) = -1
            myButton.BackgroundImage = System.Drawing.Image.FromFile(imagePathPlayer2 & player2icon & ".png")
            myButton.BackgroundImageLayout = ImageLayout.Stretch
            'myButton.Text = "O"
        End If
        phase += 1
        myButton.Enabled = False

        checkCondition()

        If AI AndAlso Not finish Then
            oppenent.Learn(newPlace)
            newPlace = oppenent.choose()
            If phase Mod 2 = 0 Then
                value(newPlace.Y, newPlace.X) = 1
                'petak(newPlace.Y, newPlace.X).Text = "X"
                petak(newPlace.Y, newPlace.X).BackgroundImage = System.Drawing.Image.FromFile(imagePathPlayer1 & player1icon & ".png")
                petak(newPlace.Y, newPlace.X).BackgroundImageLayout = ImageLayout.Stretch
            Else
                value(newPlace.Y, newPlace.X) = -1
                'petak(newPlace.Y, newPlace.X).Text = "O"
                petak(newPlace.Y, newPlace.X).BackgroundImage = System.Drawing.Image.FromFile(imagePathPlayer2 & player2icon & ".png")
                petak(newPlace.Y, newPlace.X).BackgroundImageLayout = ImageLayout.Stretch
            End If

            Debug.WriteLine("x : " & newPlace.X & " | y : " & newPlace.Y)
            ' AI will do something
            phase += 1
            'petak di endabled paflse
            petak(newPlace.Y, newPlace.X).Enabled = False
            checkCondition()
            oppenent.Learn(newPlace)
        End If
    End Sub

    Private Sub checkCondition()
        Dim hitungD1 As Integer
        Dim hitungD2 As Integer
        Dim varD As Integer = ukuran - 1
        Dim draw As Boolean = True

        Debug.WriteLine("phase : " & phase)

        For i As Integer = 0 To ukuran - 1
            Dim hitung As Integer = 0

            'horizontal
            'statis : hitung = value(i, 0) + value(i, 1) + value(i, 2)
            'hitung value
            For x As Integer = 0 To ukuran - 1
                hitung += value(i, x)
            Next

            'cek hasil hitungan
            If hitung = -1 * ukuran Then
                winner = 2
                matikan()
                draw = False
                Fwin.Show()

            ElseIf hitung = ukuran Then
                winner = 1
                matikan()
                draw = False
                Fwin.Show()
            End If

            'vertikal
            'statis:hitung = value(0, i) + value(1, i) + value(2, i)
            'hitung value

            hitung = 0
            For x As Integer = 0 To ukuran - 1
                hitung += value(x, i)
            Next

            'cek hasil hitungan
            If hitung = -1 * ukuran Then

                winner = 2
                matikan()
                draw = False
                Fwin.Show()
            ElseIf hitung = ukuran Then

                winner = 1
                matikan()
                draw = False
                Fwin.Show()
            End If

            'hitung diagonal, kiriAtas->kananBawah (D1); kananAtas->kiriBawah(D2)
            hitungD1 += value(i, i)
            hitungD2 += value(i, varD)
            varD -= 1
        Next

        'cek diagonal
        If hitungD1 = -1 * ukuran Or hitungD2 = -1 * ukuran Then
            winner = 2
            matikan()
            draw = False
            Fwin.Show()
        ElseIf hitungD1 = ukuran Or hitungD2 = ukuran Then
            winner = 1
            matikan()
            draw = False
            Fwin.Show()
        End If

        'cek Seri(turn habis)
        If (phase >= ukuran * ukuran) And draw = True Then
            'MessageBox.Show("Kedua pemain imbang")
            matikan()
            winner = 0
            Fwin.Show()
        End If
    End Sub

    Private Sub matikan()
        'mematikan semua petak
        For Each btnPetak In petak
            btnPetak.Enabled = False
            finish = True
        Next
    End Sub

    Private Sub kosongi()
        Me.Size = New Size(269, 87)
        ukuran = -1
    End Sub

    Private Sub F3x3_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Application.Exit()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnGiveup.Click
        If phase Mod 2 = 0 Then
            winner = 2
            Fwin.Show()
        Else
            winner = 1
            Fwin.Show()
        End If
        matikan()
    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        Me.resetBoard()
    End Sub

    Public Sub resetBoard()
        Panel1.Controls.Clear()
        For i As Integer = 0 To ukuran - 1
            For j As Integer = 0 To ukuran - 1
                value(i, j) = 0
            Next
        Next

        finish = False
        phase = 0

        If AI_On() Then
            Debug.WriteLine("ai on")
            Select Case difficulty
                Case 1 : oppenent = New EasyArtificialIntelligence(ukuran, value)
                Case 2 : oppenent = New ArtificialIntelligence(ukuran, value)
                Case 3 : oppenent = New HardArtificialIntelligence(ukuran, value)
            End Select
            Debug.WriteLine(difficulty)
        End If

        createField()
    End Sub
End Class