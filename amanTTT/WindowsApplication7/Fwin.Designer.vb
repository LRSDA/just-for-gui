﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Fwin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.stateWin = New System.Windows.Forms.Label()
        Me.btPlayAgain = New System.Windows.Forms.Button()
        Me.btExit = New System.Windows.Forms.Button()
        Me.theWinner = New System.Windows.Forms.Label()
        Me.stateDraw = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'stateWin
        '
        Me.stateWin.Font = New System.Drawing.Font("Candara", 60.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.stateWin.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.stateWin.Location = New System.Drawing.Point(3, 70)
        Me.stateWin.Name = "stateWin"
        Me.stateWin.Size = New System.Drawing.Size(576, 97)
        Me.stateWin.TabIndex = 0
        Me.stateWin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btPlayAgain
        '
        Me.btPlayAgain.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btPlayAgain.Font = New System.Drawing.Font("Candara", 27.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btPlayAgain.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btPlayAgain.Location = New System.Drawing.Point(328, 493)
        Me.btPlayAgain.Name = "btPlayAgain"
        Me.btPlayAgain.Size = New System.Drawing.Size(197, 74)
        Me.btPlayAgain.TabIndex = 17
        Me.btPlayAgain.Text = "Play Again"
        Me.btPlayAgain.UseVisualStyleBackColor = False
        '
        'btExit
        '
        Me.btExit.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btExit.Font = New System.Drawing.Font("Candara", 27.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btExit.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btExit.Location = New System.Drawing.Point(63, 493)
        Me.btExit.Name = "btExit"
        Me.btExit.Size = New System.Drawing.Size(197, 74)
        Me.btExit.TabIndex = 16
        Me.btExit.Text = "Exit"
        Me.btExit.UseVisualStyleBackColor = False
        '
        'theWinner
        '
        Me.theWinner.Font = New System.Drawing.Font("Candara", 50.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.theWinner.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.theWinner.Location = New System.Drawing.Point(-1, 169)
        Me.theWinner.Name = "theWinner"
        Me.theWinner.Size = New System.Drawing.Size(580, 82)
        Me.theWinner.TabIndex = 19
        Me.theWinner.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'stateDraw
        '
        Me.stateDraw.Font = New System.Drawing.Font("Candara", 60.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.stateDraw.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.stateDraw.Location = New System.Drawing.Point(0, 267)
        Me.stateDraw.Name = "stateDraw"
        Me.stateDraw.Size = New System.Drawing.Size(579, 97)
        Me.stateDraw.TabIndex = 20
        Me.stateDraw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Fwin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(580, 685)
        Me.Controls.Add(Me.stateDraw)
        Me.Controls.Add(Me.theWinner)
        Me.Controls.Add(Me.btPlayAgain)
        Me.Controls.Add(Me.btExit)
        Me.Controls.Add(Me.stateWin)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Fwin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Fwin"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents stateWin As Label
    Friend WithEvents btPlayAgain As Button
    Friend WithEvents btExit As Button
    Friend WithEvents theWinner As Label
    Friend WithEvents stateDraw As Label
End Class
