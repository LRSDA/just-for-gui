﻿Public Class Generate_Board
    Dim phase As Integer = 0
    Dim petak(,) As Button
    Dim value(,) As Integer
    Dim ukuran As Integer

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'ulangi
        Panel1.Controls.Clear()
        For i As Integer = 0 To ukuran - 1
            For j As Integer = 0 To ukuran - 1
                value(i, j) = 0
            Next
        Next

        'cek ukuran apa yang dipilih dan berapa jumlah menang yang dibutuhkan
        Dim pilihan As Integer = ComboBox1.SelectedIndex
        Dim sizePetak As Integer = 75
        phase = 0

        Select Case pilihan
            Case -1
                MessageBox.Show("Pilih salah satu ukuran papan")
                kosongi()
                Exit Sub
            Case 0
                ukuran = 3
            Case 1
                ukuran = 5
                sizePetak = 70
            Case 2
                ukuran = 9
                sizePetak = 50
        End Select

        'setting ukuran form dan panel
        Me.Size = New Size((18 + (sizePetak + 6) * ukuran) + 16, (18 + (sizePetak + 6) * ukuran) + 72)
        Panel1.Size = New Size((sizePetak * ukuran) + (6 * (ukuran + 1)), (sizePetak * ukuran) + (6 * (ukuran + 1)))

        'mengatur ukuran array
        ReDim value(ukuran - 1, ukuran - 1)
        ReDim petak(ukuran - 1, ukuran - 1)

        'generate petak
        For i As Integer = 0 To ukuran - 1
            For j As Integer = 0 To ukuran - 1
                petak(i, j) = New Button()
                Panel1.Controls.Add(petak(i, j))
                'edit tampilan button disini
                petak(i, j).Width = sizePetak
                petak(i, j).Height = sizePetak
                petak(i, j).Location = New Point(10 + j * (sizePetak + 6), 6 + i * (sizePetak + 6))
                petak(i, j).Name = "petak" & CStr(i) & CStr(j)
                petak(i, j).Visible = True
                AddHandler petak(i, j).Click, AddressOf Me.btnPetak_Click
            Next
        Next
    End Sub

    Private Sub btnPetak_Click(sender As Object, e As EventArgs)
        'event button di klik
        Dim myButton = DirectCast(sender, Button)
        If phase Mod 2 = 0 Then
            value(CInt(myButton.Name.Substring(5, 1)), CInt(myButton.Name.Substring(6, 1))) = -1
            myButton.Text = "X"
        Else
            value(CInt(myButton.Name.Substring(5, 1)), CInt(myButton.Name.Substring(6, 1))) = 1
            myButton.Text = "O"
        End If
        phase += 1
        myButton.Enabled = False

        checkCondition()
    End Sub

    Private Sub checkCondition()
        Dim hitungD1 As Integer
        Dim hitungD2 As Integer
        Dim varD As Integer = ukuran - 1
        Dim draw As Boolean = True

        For i As Integer = 0 To ukuran - 1
            Dim hitung As Integer = 0

            'horizontal
            'statis : hitung = value(i, 0) + value(i, 1) + value(i, 2)
            'hitung value
            For x As Integer = 0 To ukuran - 1
                hitung += value(i, x)
            Next

            'cek hasil hitungan
            If hitung = -1 * ukuran Then
                MessageBox.Show("Player X menang!")
                matikan()
                draw = False
            ElseIf hitung = ukuran Then
                MessageBox.Show("Player O menang!")
                matikan()
                draw = False
            End If

            'vertikal
            'statis:hitung = value(0, i) + value(1, i) + value(2, i)
            'hitung value

            hitung = 0
            For x As Integer = 0 To ukuran - 1
                hitung += value(x, i)
            Next

            'cek hasil hitungan
            If hitung = -1 * ukuran Then
                MessageBox.Show("Player X menang!")
                matikan()
                draw = False
            ElseIf hitung = ukuran Then
                MessageBox.Show("Player O menang!")
                matikan()
                draw = False
            End If

            'hitung diagonal, kiriAtas->kananBawah (D1); kananAtas->kiriBawah(D2)
            hitungD1 += value(i, i)
            hitungD2 += value(i, varD)
            varD -= 1
        Next

        'cek diagonal
        If hitungD1 = -1 * ukuran Or hitungD2 = -1 * ukuran Then
            MessageBox.Show("Player X menang!")
            matikan()
            draw = False
        ElseIf hitungD1 = ukuran Or hitungD2 = ukuran Then
            MessageBox.Show("Player O menang!")
            matikan()
            draw = False
        End If

        'cek Seri(turn habis)
        If (phase >= ukuran * ukuran) And draw = True Then
            MessageBox.Show("Kedua pemain imbang")
        End If
    End Sub

    Private Sub matikan()
        'mematikan semua petak
        For Each btnPetak In petak
            btnPetak.Enabled = False
        Next
    End Sub

    Private Sub kosongi()
        Me.Size = New Size(269, 87)
        ComboBox1.SelectedIndex = -1
    End Sub
End Class