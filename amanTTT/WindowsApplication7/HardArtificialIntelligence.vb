﻿Imports WindowsApplication7

Public Class HardArtificialIntelligence
    Inherits ArtificialIntelligence

    Dim possibility As New ArrayList
    Dim numChance As Integer = 0
    Dim phase As Integer = 0

    Public Sub New(lengthBoard As Integer, ByRef board(,) As Integer)
        MyBase.New(lengthBoard, board)
    End Sub

    Public Overrides Sub Learn(enemyMove As Coordinate)
        Dim bufferMax As Integer = 0
        Me.board = board
        Dim caution = 0

        heuristicModel(enemyMove.Y, enemyMove.X) = 0


        'Vertical
        For i = 0 To lengthBoard - 1
            If heuristicModel(i, enemyMove.X) = 0 AndAlso checkCoordinate(New Coordinate(enemyMove.X, i)) Then
                Continue For
            End If
            Debug.WriteLine(i & "V")
            updateHMVertical(New Coordinate(enemyMove.X, i))
        Next


        'Horizontal
        For i = 0 To lengthBoard - 1
            If heuristicModel(enemyMove.Y, i) = 0 AndAlso checkCoordinate(New Coordinate(i, enemyMove.Y)) Then
                Continue For
            End If
            Debug.WriteLine(i & "H")
            updateHMHorizontal(New Coordinate(i, enemyMove.Y))
        Next

        'Diagonal left
        If (enemyMove.isDL()) Then
            For i = 0 To lengthBoard - 1
                If heuristicModel(i, i) = 0 AndAlso checkCoordinate(New Coordinate(i, i)) Then
                    caution += 1
                    If caution = 3 Then
                        updateHMDiagonalLtoR(New Coordinate(i, i))
                    End If
                    Continue For
                End If
                Debug.WriteLine(i & "DL")

                updateHMDiagonalLtoR(New Coordinate(i, i))
            Next
        End If
        Debug.WriteLine(enemyMove.isDL().ToString)

        caution = 0

        'Diagonal right
        If enemyMove.isDR(lengthBoard) Then
            Dim j As Integer = (lengthBoard - 1)
            For i = 0 To lengthBoard - 1
                If heuristicModel(j, i) = 0 AndAlso checkCoordinate(New Coordinate(i, j)) Then
                    caution += 1
                    If caution = 3 Then
                        updateHMDiagonalRtoL(New Coordinate(i, j))
                    End If
                    j -= 1
                    Continue For
                End If
                Debug.WriteLine(i & "DR")
                updateHMDiagonalRtoL(New Coordinate(i, j))
                j -= 1
            Next
        End If
        Debug.WriteLine(enemyMove.isDR(lengthBoard).ToString)

        For i = 0 To lengthBoard - 1
            For j = 0 To lengthBoard - 1
                If heuristicModel(i, j) > bufferMax Then

                    bufferMax = heuristicModel(i, j)
                End If
            Next
        Next

        heuristicMaxPoint = bufferMax

        For i = 0 To lengthBoard - 1
            For j = 0 To lengthBoard - 1
                Debug.Write("| " & heuristicModel(i, j) & "|")
            Next
            Debug.WriteLine("")
        Next

        Debug.WriteLine("")

        For i = 0 To lengthBoard - 1
            For j = 0 To lengthBoard - 1
                Debug.Write("| " & board(i, j) & "|")
            Next
            Debug.WriteLine("")
        Next

        For i = 0 To lengthBoard - 1
            For j = 0 To lengthBoard - 1
                If heuristicModel(i, j) = heuristicMaxPoint Then
                    numChance += 0
                End If
            Next
        Next

        phase += 1
    End Sub

    Public Overrides Function choose() As Coordinate

        If heuristicMaxPoint = 0 Then
        ElseIf numChance > 1 Then
            Return predict()
        Else
            For i = 0 To lengthBoard - 1
                For j = 0 To lengthBoard - 1
                    If heuristicModel(i, j) = heuristicMaxPoint Then
                        Return New Coordinate(j, i)
                    End If
                Next
            Next
        End If
        Return New Coordinate(lengthBoard - 1, lengthBoard - 1)
    End Function

    Public Overrides Sub updateHMDiagonalLtoR(target As Coordinate)
        Dim humanCounter As Integer = 0
        Dim aiCounter As Integer = 0
        For I As Integer = 0 To lengthBoard - 1
            If board(I, I) = 1 Then
                humanCounter += 1
            ElseIf board(I, I) = -1 Then
                aiCounter += 1
            End If
        Next

        If humanCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 3
        ElseIf aiCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 10
        ElseIf humanCounter = 1 AndAlso aiCounter = 0 Then
            heuristicModel(target.Y, target.X) += 1
        ElseIf aiCounter = humanCounter And aiCounter = 1 Then
            heuristicModel(target.Y, target.X) -= 2
        End If

        If (aiCounter + humanCounter) = 3 AndAlso board(1, 1) = -1 Then
            Debug.WriteLine("danger")
            If Not heuristicModel(0, 1) = 0 Then
                heuristicModel(0, 1) += 3
            End If

            If Not heuristicModel(1, 0) = 0 Then
                heuristicModel(1, 0) += 3
            End If

            If Not heuristicModel(1, 2) = 0 Then
                heuristicModel(1, 2) += 3
            End If

            If Not heuristicModel(2, 1) = 0 Then
                heuristicModel(2, 1) += 3
            End If
        End If

        If (aiCounter + humanCounter) = 3 Then
            heuristicModel(target.Y, target.X) = 0
        End If
    End Sub

    Public Overrides Sub updateHMDiagonalRtoL(target As Coordinate)
        Dim humanCounter As Integer = 0
        Dim aiCounter As Integer = 0
        Dim J As Integer = lengthBoard - 1
        For I As Integer = 0 To lengthBoard - 1
            If board(J, I) = 1 Then
                humanCounter += 1
            ElseIf board(J, I) = -1 Then
                aiCounter += 1
            End If
            J -= 1
        Next

        If humanCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 3
        ElseIf aiCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 10
        ElseIf humanCounter = 1 AndAlso aiCounter = 0 Then
            heuristicModel(target.Y, target.X) += 1
        ElseIf aiCounter = humanCounter And aiCounter = 1 Then
            heuristicModel(target.Y, target.X) -= 2
        End If

        If (aiCounter + humanCounter) = 3 AndAlso humanCounter = 2 AndAlso board(1, 1) = -1 Then
            Debug.WriteLine("danger")
            If Not heuristicModel(0, 1) = 0 Then
                heuristicModel(0, 1) += 3
            End If

            If Not heuristicModel(1, 0) = 0 Then
                heuristicModel(1, 0) += 3
            End If

            If Not heuristicModel(1, 2) = 0 Then
                heuristicModel(1, 2) += 3
            End If

            If Not heuristicModel(2, 1) = 0 Then
                heuristicModel(2, 1) += 3
            End If
        End If

        If (aiCounter + humanCounter) = 3 Then
            heuristicModel(target.Y, target.X) = 0
        End If
    End Sub

    Function predict() As Coordinate
        possibility.Clear()
        Dim minimum As Integer = 10000

        For i = 0 To lengthBoard - 1
            For j = 0 To lengthBoard - 1
                If heuristicModel(i, j) = heuristicMaxPoint Then
                    possibility.Add(New Chance(New Coordinate(j, i), board, heuristicModel, phase))
                End If
            Next
        Next

        For i = 0 To possibility.Count
            Dim c As Chance = possibility.Item(i)
            c.predict()
            If c._point < minimum Then
                minimum = c._point
            End If
        Next

        For i = 0 To possibility.Count
            Dim c As Chance = possibility.Item(i)
            c.predict()
            If c._point = minimum Then
                Return c._step
            End If
        Next

        Return New Coordinate(0, 0)
    End Function
End Class
