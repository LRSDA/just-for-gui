﻿Public Class Coordinate
    Private _x As Integer
    Private _y As Integer

    Sub New(ByVal x As Integer, ByVal y As Integer)
        Me._x = x
        Me._y = y
    End Sub

    Public Property X As Integer
        Get
            Return _x
        End Get
        Set(value As Integer)
            Me._x = value
        End Set
    End Property

    Public Property Y As Integer
        Get
            Return _y
        End Get
        Set(value As Integer)
            Me._y = value
        End Set
    End Property

    Function isDL() As Boolean
        Return _x = _y
    End Function

    Function isDR(ByVal lengthboard As Integer) As Boolean
        Return _y + _x = lengthboard - 1
    End Function


End Class
