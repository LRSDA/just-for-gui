﻿Imports WindowsApplication7.Setting

Public Class Fsetting

    Private Sub btOk_Click(sender As Object, e As EventArgs) Handles btOk.Click
        PlaySoundEffectOnce()
        player1name = tb1.Text
        player2name = tb2.Text
        'Debug.WriteLine(player1name)

        If Not CheckBox1.Checked() Or bgm Then
            PlayLoopingBackgroundSoundFile()
        End If

        Fhome.Show()
        Me.Hide()

    End Sub

    Private Sub player11_Click(sender As Object, e As EventArgs) Handles player11.Click
        PlaySoundEffectOnce()
        If player11.BackColor = Color.Red Then
            'player11.BackColor = Color.FromArgb(100, 255, 192, 192)
        Else
            player11.BackColor = Color.Red
            player12.BackColor = Color.FromArgb(100, 255, 192, 192)
            player1icon = 1
        End If
    End Sub

    Private Sub player12_Click(sender As Object, e As EventArgs) Handles player12.Click
        PlaySoundEffectOnce()
        If player12.BackColor = Color.Red Then
            'player12.BackColor = Color.FromArgb(100, 255, 192, 192)
        Else
            player12.BackColor = Color.Red
            player11.BackColor = Color.FromArgb(100, 255, 192, 192)
            player1icon = 2
        End If
    End Sub

    Private Sub player21_Click(sender As Object, e As EventArgs) Handles player21.Click
        PlaySoundEffectOnce()
        If player21.BackColor = Color.Red Then
            'player21.BackColor = Color.FromArgb(100, 255, 192, 192)
        Else
            player21.BackColor = Color.Red
            player22.BackColor = Color.FromArgb(100, 255, 192, 192)
            player2icon = 1
        End If
    End Sub

    Private Sub player22_Click(sender As Object, e As EventArgs) Handles player22.Click
        PlaySoundEffectOnce()
        If player22.BackColor = Color.Red Then
            'player22.BackColor = Color.FromArgb(100, 255, 192, 192)
        Else
            player22.BackColor = Color.Red
            player21.BackColor = Color.FromArgb(100, 255, 192, 192)
            player2icon = 2
        End If
    End Sub

    Private Sub btCancel_Click(sender As Object, e As EventArgs) Handles btCancel.Click
        'btCancel.
        PlaySoundEffectOnce()
        Fhome.Show()
        Me.Hide()
    End Sub

    Private Sub btEasy_Click(sender As Object, e As EventArgs) Handles btEasy.Click
        PlaySoundEffectOnce()
        difficulty = 1
        '100, 0, 192, 192 biru
        '100, 128, 255, 128 ijo
        If btEasy.BackColor = Color.FromArgb(255, 128, 255, 128) Then
        Else
            btEasy.BackColor = Color.FromArgb(255, 128, 255, 128)
            btMedium.BackColor = Color.FromArgb(255, 0, 192, 192)
            btHard.BackColor = Color.FromArgb(255, 0, 192, 192)
        End If
    End Sub

    Private Sub btMedium_Click(sender As Object, e As EventArgs) Handles btMedium.Click
        PlaySoundEffectOnce()
        difficulty = 2
        If btMedium.BackColor = Color.FromArgb(255, 128, 255, 128) Then
        Else
            btMedium.BackColor = Color.FromArgb(255, 128, 255, 128)
            btEasy.BackColor = Color.FromArgb(255, 0, 192, 192)
            btHard.BackColor = Color.FromArgb(255, 0, 192, 192)
        End If
    End Sub

    Private Sub btHard_Click(sender As Object, e As EventArgs) Handles btHard.Click
        PlaySoundEffectOnce()
        difficulty = 3
        If btHard.BackColor = Color.FromArgb(255, 128, 255, 128) Then
        Else
            btHard.BackColor = Color.FromArgb(255, 128, 255, 128)
            btEasy.BackColor = Color.FromArgb(255, 0, 192, 192)
            btMedium.BackColor = Color.FromArgb(255, 0, 192, 192)
        End If
    End Sub


    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged
        PlaySoundEffectOnce()
        If CheckBox1.Checked Then
            bgm = True
        Else
            bgm = False
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox2.CheckedChanged
        PlaySoundEffectOnce()
        If CheckBox2.Checked Then
            sfx = True
        Else
            sfx = False
        End If
    End Sub

    Private Sub Fsetting_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class