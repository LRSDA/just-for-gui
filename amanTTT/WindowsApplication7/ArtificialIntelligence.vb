﻿Imports WindowsApplication7

Public Class ArtificialIntelligence

    Public heuristicModel(,) As Integer
    Public heuristicMaxPoint As Integer
    Public board(,) As Integer
    Public lengthBoard As Integer


    Sub New(ByVal lengthBoard As Integer, ByRef board(,) As Integer)
        Me.board = board
        Me.lengthBoard = lengthBoard
        If lengthBoard = 3 Then
            heuristicModel = New Integer(2, 2) {{3, 2, 3}, {2, 4, 2}, {3, 2, 3}}
            heuristicMaxPoint = 4
        ElseIf lengthBoard = 5 Then
            heuristicModel = New Integer(4, 4) {{3, 2, 2, 2, 3}, {2, 2, 2, 2, 2}, {2, 2, 4, 2, 2}, {2, 2, 2, 2, 2}, {3, 2, 2, 2, 3}}
            heuristicMaxPoint = 4
        ElseIf lengthBoard = 9 Then
            heuristicModel = New Integer(8, 8) {{3, 2, 2, 2, 2, 2, 2, 2, 3}, {2, 2, 2, 2, 2, 2, 2, 2, 2}, {2, 2, 2, 2, 2, 2, 2, 2, 2}, {2, 2, 2, 2, 2, 2, 2, 2, 2}, {2, 2, 2, 2, 4, 2, 2, 2, 2}, {2, 2, 2, 2, 2, 2, 2, 2, 2}, {2, 2, 2, 2, 2, 2, 2, 2, 2}, {2, 2, 2, 2, 2, 2, 2, 2, 2}, {3, 2, 2, 2, 2, 2, 2, 2, 3}}
            heuristicMaxPoint = 4
        End If
    End Sub

    Function checkCoordinate(ByVal enemyMove As Coordinate) As Boolean
        If board(enemyMove.Y, enemyMove.X) = 0 Then
            Return False
        End If
        Return True
    End Function

    Overridable Sub Learn(ByVal enemyMove As Coordinate)
        Dim bufferMax As Integer = 0
        Me.board = board

        heuristicModel(enemyMove.Y, enemyMove.X) = 0


        'Vertical
        For i = 0 To lengthBoard - 1
            If heuristicModel(i, enemyMove.X) = 0 AndAlso checkCoordinate(New Coordinate(enemyMove.X, i)) Then
                Continue For
            End If
            Debug.WriteLine(i & "V")
            updateHMVertical(New Coordinate(enemyMove.X, i))
        Next


        'Horizontal
        For i = 0 To lengthBoard - 1
            If heuristicModel(enemyMove.Y, i) = 0 AndAlso checkCoordinate(New Coordinate(i, enemyMove.Y)) Then
                Continue For
            End If
            Debug.WriteLine(i & "H")
            updateHMHorizontal(New Coordinate(i, enemyMove.Y))
        Next

        'Diagonal left
        If (enemyMove.isDL()) Then
            For i = 0 To lengthBoard - 1
                If heuristicModel(i, i) = 0 AndAlso checkCoordinate(New Coordinate(i, i)) Then
                    Continue For
                End If
                Debug.WriteLine(i & "DL")
                updateHMDiagonalLtoR(New Coordinate(i, i))
            Next
        End If
        Debug.WriteLine(enemyMove.isDL().ToString)

        'Diagonal right
        If enemyMove.isDR(lengthBoard) Then
            Dim j As Integer = (lengthBoard - 1)
            For i = 0 To lengthBoard - 1
                If heuristicModel(j, i) = 0 AndAlso checkCoordinate(New Coordinate(i, j)) Then
                    j -= 1
                    Continue For
                End If
                Debug.WriteLine(i & "DR")
                updateHMDiagonalRtoL(New Coordinate(i, j))
                j -= 1
            Next
        End If
        Debug.WriteLine(enemyMove.isDR(lengthBoard).ToString)

        For i = 0 To lengthBoard - 1
            For j = 0 To lengthBoard - 1
                If heuristicModel(i, j) > bufferMax Then

                    bufferMax = heuristicModel(i, j)
                End If
            Next
        Next

        heuristicMaxPoint = bufferMax

        For i = 0 To lengthBoard - 1
            For j = 0 To lengthBoard - 1
                Debug.Write("| " & heuristicModel(i, j) & "|")
            Next
            Debug.WriteLine("")
        Next

        Debug.WriteLine("")

        For i = 0 To lengthBoard - 1
            For j = 0 To lengthBoard - 1
                Debug.Write("| " & board(i, j) & "|")
            Next
            Debug.WriteLine("")
        Next

    End Sub

    Overridable Sub updateHMHorizontal(ByVal target As Coordinate)
        Dim humanCounter As Integer = 0
        Dim aiCounter As Integer = 0
        For i As Integer = 0 To lengthBoard - 1
            If board(target.Y, i) = 1 Then
                humanCounter += 1
            ElseIf board(target.Y, i) = -1 Then
                aiCounter += 1
            End If
        Next

        If humanCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 3
        ElseIf aiCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 10
        ElseIf humanCounter = 1 AndAlso aiCounter = 0 Then
            heuristicModel(target.Y, target.X) += 1
        ElseIf aiCounter = humanCounter And aiCounter = 1 Then
            heuristicModel(target.Y, target.X) -= 2
        End If
    End Sub

    Overridable Sub updateHMVertical(ByVal target As Coordinate)
        Dim humanCounter As Integer = 0
        Dim aiCounter As Integer = 0
        For I As Integer = 0 To lengthBoard - 1
            If board(I, target.X) = 1 Then
                humanCounter += 1
            ElseIf board(I, target.X) = -1 Then
                aiCounter += 1
            End If
        Next

        If humanCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 3
        ElseIf aiCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 10
        ElseIf humanCounter = 1 AndAlso aiCounter = 0 Then
            heuristicModel(target.Y, target.X) += 1
        ElseIf aiCounter = humanCounter And aiCounter = 1 Then
            heuristicModel(target.Y, target.X) -= 2
        End If

    End Sub

    Overridable Sub updateHMDiagonalLtoR(ByVal target As Coordinate)
        Dim humanCounter As Integer = 0
        Dim aiCounter As Integer = 0
        For I As Integer = 0 To lengthBoard - 1
            If board(I, I) = 1 Then
                humanCounter += 1
            ElseIf board(I, I) = -1 Then
                aiCounter += 1
            End If
        Next

        If humanCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 3
        ElseIf aiCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 10
        ElseIf humanCounter = 1 AndAlso aiCounter = 0 Then
            heuristicModel(target.Y, target.X) += 1
        ElseIf aiCounter = humanCounter And aiCounter = 1 Then
            heuristicModel(target.Y, target.X) -= 2
        End If

    End Sub

    Overridable Sub updateHMDiagonalRtoL(ByVal target As Coordinate)
        Dim humanCounter As Integer = 0
        Dim aiCounter As Integer = 0
        Dim J As Integer = lengthBoard - 1
        For I As Integer = 0 To lengthBoard - 1
            If board(J, I) = 1 Then
                humanCounter += 1
            ElseIf board(J, I) = -1 Then
                aiCounter += 1
            End If
            J -= 1
        Next

        If humanCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 3
        ElseIf aiCounter = lengthBoard - 1 Then
            heuristicModel(target.Y, target.X) += 10
        ElseIf humanCounter = 1 AndAlso aiCounter = 0 Then
            heuristicModel(target.Y, target.X) += 1
        ElseIf aiCounter = humanCounter And aiCounter = 1 Then
            heuristicModel(target.Y, target.X) -= 2
        End If

    End Sub

    Overridable Function choose() As Coordinate
        If heuristicMaxPoint = 0 Then
            Return checkBoard()
        Else
            For i = 0 To lengthBoard - 1
                For j = 0 To lengthBoard - 1
                    If heuristicModel(i, j) = heuristicMaxPoint Then
                        Return New Coordinate(j, i)
                    End If
                Next
            Next
        End If
        Return New Coordinate(lengthBoard - 1, lengthBoard - 1)
    End Function

    Function checkBoard() As Coordinate
        For i = 0 To lengthBoard - 1
            For j = 0 To lengthBoard - 1
                If board(i, j) = 0 Then
                    Return New Coordinate(j, i)
                End If
            Next
        Next
        Return New Coordinate(lengthBoard - 1, lengthBoard - 1)
    End Function

End Class
