﻿Public Class Fwin
    Private Sub Fwin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Classic.Hide()
        If winner = 1 Then
            stateWin.Text = "The WINNER is"
            theWinner.Text = player1name
        ElseIf winner = 2 Then
            stateWin.Text = "The WINNER is"
            theWinner.Text = player2name
        Else
            stateDraw.Text = "DRAW"
        End If
    End Sub

    Private Sub btExit_Click(sender As Object, e As EventArgs) Handles btExit.Click
        PlaySoundEffectOnce()
        Application.Exit()
    End Sub

    Private Sub btPlayAgain_Click(sender As Object, e As EventArgs) Handles btPlayAgain.Click
        PlaySoundEffectOnce()
        Me.Close()
        Fhome.Show()
    End Sub
End Class