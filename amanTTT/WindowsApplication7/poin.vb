﻿Public Class poin
    Dim phase As Integer = 0
    Dim petak(,) As Button
    Dim value(,) As Integer
    Dim finish As Boolean = False
    Dim pointx As Integer = 0
    Dim pointo As Integer = 0

    Private Sub poin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblPlayer1.Text = player1name
        lblPlayer2.Text = player2name

        Debug.WriteLine(player1name)
        Me.MaximizeBox = False

        'mengatur ukuran array
        ReDim value(ukuran - 1, ukuran - 1)
        ReDim petak(ukuran - 1, ukuran - 1)

        createField()
        resetBoard()
    End Sub

    Private Sub createField()
        Dim leftMargin As Integer = 20 '80
        Dim sizePetak As Integer = 75
        phase = 0

        Debug.WriteLine("" & ukuran & "xxx ")

        Select Case ukuran
            Case -1
                MessageBox.Show("Pilih salah satu ukuran papan")
                kosongi()
                Exit Sub
            Case 3
                sizePetak = 165 '120
            Case 5
                sizePetak = 96 '70
            Case 9
                sizePetak = 50 '45
                leftMargin = 25 '20
        End Select

        'cek ukuran apa yang dipilih dan berapa jumlah menang yang dibutuhkan

        'setting ukuran form dan panel
        Me.Size = New Size(Me.Width, (18 + (sizePetak + 6) * ukuran) + 16)
        'Panel1.Size = New Size((sizePetak * ukuran) + (6 * (ukuran + 1)), (sizePetak * ukuran) + (6 * (ukuran + 1)))



        'generate petak
        For i As Integer = 0 To ukuran - 1
            For j As Integer = 0 To ukuran - 1
                petak(i, j) = New Button()
                Panel1.Controls.Add(petak(i, j))
                'edit tampilan button disini
                petak(i, j).Width = sizePetak
                petak(i, j).Font = New Font(Me.Font.FontFamily, Convert.ToSingle(Convert.ToDouble(sizePetak) / 2), FontStyle.Bold)
                petak(i, j).Height = sizePetak
                petak(i, j).Location = New Point(leftMargin + j * (sizePetak + 6), 6 + i * (sizePetak + 6))
                petak(i, j).Name = "petak" & CStr(i) & CStr(j)
                petak(i, j).Visible = True
                AddHandler petak(i, j).Click, AddressOf Me.btnPetak_Click
            Next
        Next
    End Sub


    Private Sub btnPetak_Click(sender As Object, e As EventArgs)
        PlaySoundEffectOnce()
        'event button di klik
        Dim myButton = DirectCast(sender, Button)
        Dim newPlace As Coordinate = New Coordinate(CInt(myButton.Name.Substring(6, 1)), CInt(myButton.Name.Substring(5, 1)))


        If phase Mod 2 = 0 Then
            value(newPlace.Y, newPlace.X) = 1
            myButton.BackgroundImage = System.Drawing.Image.FromFile(imagePathPlayer1 & player1icon & ".png")
            myButton.BackgroundImageLayout = ImageLayout.Stretch

            'myButton.Text = "X"
        Else
            value(newPlace.Y, newPlace.X) = -1
            myButton.BackgroundImage = System.Drawing.Image.FromFile(imagePathPlayer2 & player2icon & ".png")
            myButton.BackgroundImageLayout = ImageLayout.Stretch
            'myButton.Text = "O"
        End If
        phase += 1
        myButton.Enabled = False

        CheckCondition2(newPlace)

        updatePoint()

        For i As Integer = 0 To ukuran - 1
            Debug.WriteLine("")
            For j As Integer = 0 To ukuran - 1
                Debug.Write("|" & value(i, j) & "|")
            Next
        Next
        Debug.WriteLine(pointo & "")
        Debug.WriteLine(pointx & "")
    End Sub

    Sub updatePoint()
        btnScore1.Text = pointx
        btnScore2.Text = pointo
    End Sub

    Sub CheckCondition2(ByVal x As Coordinate)
        pointo = 0
        pointx = 0

        cv(x)
        ch(x)
        cdl(x)
        cdr(x)

        If full() Then

            If pointx > pointo Then
                winner = 1
                matikan()
                Fwin.Show()
                Me.Close()
            ElseIf pointo > pointx Then
                winner = 2
                matikan()
                Fwin.Show()
                Me.Close()
            Else
                winner = 0
                matikan()
                Fwin.Show()
                Me.Close()
            End If
        End If


    End Sub


    Sub cv(ByVal newplace As Coordinate)
        Dim temp As Integer = 0
        Dim valtamp As Integer = 0
        For i As Integer = 0 To ukuran - 1
            temp = 0
            valtamp = 0
            For j As Integer = 0 To ukuran - 1
                If Not value(j, i) = valtamp Then
                    temp = 1
                    valtamp = value(j, i)
                Else
                    temp += 1
                End If


                If temp = 3 AndAlso valtamp = 1 Then
                    temp = 1
                    pointx += 1
                ElseIf temp = 3 AndAlso valtamp = -1 Then
                    temp = 1
                    pointo += 1
                End If
            Next
        Next
    End Sub

    Sub ch(ByVal newplace As Coordinate)
        Dim temp As Integer = 0
        Dim valtamp As Integer = 0
        For i As Integer = 0 To ukuran - 1
            temp = 0
            valtamp = 0
            For j As Integer = 0 To ukuran - 1
                If Not value(i, j) = valtamp Then
                    temp = 1
                    valtamp = value(i, j)
                Else
                    temp += 1
                End If


                If temp = 3 AndAlso valtamp = 1 Then
                    temp = 1
                    pointx += 1
                ElseIf temp = 3 AndAlso valtamp = -1 Then
                    temp = 1
                    pointo += 1
                End If
            Next
        Next
    End Sub

    Sub cdr(ByVal newplace As Coordinate)
        Dim temp As Integer = 0
        Dim valtamp As Integer = 0
        Dim x As Integer = ukuran - 1
        Dim y As Integer = ukuran - 1
        Dim lengthloop As Integer = 0

        While Not x = 0 Or Not y = 0
            Debug.WriteLine("trying do CDR")
            temp = 0
            valtamp = 0
            If y > 0 AndAlso Not y = x Then
                lengthloop = (ukuran - 1) - y
            ElseIf x > 0 AndAlso Not y = x Then
                lengthloop = x
            Else
                lengthloop = 0
            End If

            For i As Integer = 0 To lengthloop
                Debug.WriteLine("y = " & y + i & "| x= " & x - i & "| ll = " & lengthloop)
                If Not value(y + i, x - i) = valtamp Then
                    temp = 1
                    valtamp = value(y + i, x - i)
                Else
                    temp += 1
                End If


                If temp = 3 AndAlso valtamp = 1 Then
                    temp = 1
                    pointx += 1
                ElseIf temp = 3 AndAlso valtamp = -1 Then
                    temp = 1
                    pointo += 1
                End If
            Next

            If y > 0 Then
                y -= 1
            Else
                x -= 1
            End If
        End While
    End Sub

    Sub cdl(ByVal newplace As Coordinate)
        Dim temp As Integer = 0
        Dim valtamp As Integer = 0
        Dim x As Integer = 0
        Dim y As Integer = ukuran - 1
        Dim lengthloop As Integer = 0

        While Not x = ukuran - 1 Or Not y = 0
            Debug.WriteLine("trying do CDL")
            temp = 0
            valtamp = 0
            If y > x Then
                lengthloop = (ukuran - 1) - y
            ElseIf x > y Then
                lengthloop = (ukuran - 1) - x
            Else
                lengthloop = ukuran - 1
            End If

            For i As Integer = 0 To lengthloop
                Debug.WriteLine("y = " & y + i & "| x= " & x + i & "| ll = " & lengthloop)
                If Not value(y + i, x + i) = valtamp Then
                    temp = 1
                    valtamp = value(y + i, x + i)
                Else
                    temp += 1
                End If


                If temp = 3 AndAlso valtamp = 1 Then
                    temp = 1
                    pointx += 1
                ElseIf temp = 3 AndAlso valtamp = -1 Then
                    temp = 1
                    pointo += 1
                End If
            Next

            If y > x Then
                y -= 1
            Else
                x += 1
            End If
        End While
    End Sub

    Function full()
        For i As Integer = 0 To ukuran - 1
            For j As Integer = 0 To ukuran - 1
                If value(i, j) = 0 Then
                    Return False
                End If
            Next
        Next
        Return True
    End Function

    Private Sub checkCondition()
        Dim hitungD1 As Integer
        Dim hitungD2 As Integer
        Dim varD As Integer = ukuran - 1
        Dim draw As Boolean = True

        Debug.WriteLine("phase : " & phase)

        For i As Integer = 0 To ukuran - 1
            Dim hitung As Integer = 0

            'horizontal
            'statis : hitung = value(i, 0) + value(i, 1) + value(i, 2)
            'hitung value
            For x As Integer = 0 To ukuran - 1
                hitung += value(i, x)
            Next

            'cek hasil hitungan
            If hitung = -1 * ukuran Then
                MessageBox.Show("Player X menang!")
                'MessageBox.Show(player2name & " menang!")
                matikan()
                draw = False
            ElseIf hitung = ukuran Then
                MessageBox.Show("Player O menang!")
                'MessageBox.Show(player1name & " menang!")
                matikan()
                draw = False
            End If

            'vertikal
            'statis:hitung = value(0, i) + value(1, i) + value(2, i)
            'hitung value

            hitung = 0
            For x As Integer = 0 To ukuran - 1
                hitung += value(x, i)
            Next

            'cek hasil hitungan
            If hitung = -1 * ukuran Then
                'MessageBox.Show(player2name & " menang!")
                MessageBox.Show("Player X menang!")
                matikan()
                draw = False
            ElseIf hitung = ukuran Then
                'MessageBox.Show(player1name & " menang!")
                MessageBox.Show("Player O menang!")
                matikan()
                draw = False
            End If

            'hitung diagonal, kiriAtas->kananBawah (D1); kananAtas->kiriBawah(D2)
            hitungD1 += value(i, i)
            hitungD2 += value(i, varD)
            varD -= 1
        Next

        'cek diagonal
        If hitungD1 = -1 * ukuran Or hitungD2 = -1 * ukuran Then
            'MessageBox.Show(player2name & " menang!")
            MessageBox.Show("Player X menang!")
            matikan()
            draw = False
        ElseIf hitungD1 = ukuran Or hitungD2 = ukuran Then
            'MessageBox.Show(player1name & " menang!")
            MessageBox.Show("Player O menang!")
            matikan()
            draw = False
        End If

        'cek Seri(turn habis)
        If (phase >= ukuran * ukuran) And draw = True Then
            MessageBox.Show("Kedua pemain imbang")
        End If
    End Sub

    Private Sub matikan()
        'mematikan semua petak
        For Each btnPetak In petak
            btnPetak.Enabled = False
            finish = True
        Next
    End Sub

    Private Sub kosongi()
        Me.Size = New Size(269, 87)
        ukuran = -1
    End Sub

    Public Sub resetBoard()
        Panel1.Controls.Clear()
        For i As Integer = 0 To ukuran - 1
            For j As Integer = 0 To ukuran - 1
                value(i, j) = 0
            Next
        Next

        phase = 0

        'If AI_On() Then
        'Select Case AI
        'Case 1 : oppenent = New EasyArtificialIntelligence(ukuran, value)
        'Case 2 : oppenent = New ArtificialIntelligence(ukuran, value)
        'End Select
        'End If

        createField()
    End Sub

    Private Sub btnScore2_Click(sender As Object, e As EventArgs) Handles btnScore2.Click

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        resetBoard()
    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub btGiveup_Click(sender As Object, e As EventArgs) Handles btGiveup.Click
        If phase Mod 2 = 0 Then
            winner = 2
            Fwin.Show()
            Me.Close()
        Else
            winner = 1
            Fwin.Show()
            Me.Close()
        End If
        matikan()
    End Sub
End Class