﻿Imports Microsoft.VisualBasic.Devices
Imports System.Runtime.InteropServices
Imports AxWMPLib
Imports System.Media
Imports System.Threading
Imports WindowsApplication7.Multiplay

Module Setting
    Public AI As Integer = 0
    Public ukuran As Integer = 0
    Public bgm As Boolean = True
    Public sfx As Boolean = True
    Public difficulty As Integer = 1
    Public player1icon As Integer = 1
    Public player2icon As Integer = 1
    Public player1name As String = "Player 1"
    Public player2name As String = "Player 2"
    Public winner As Integer = 1
    Public hvr As Integer = 1

    Public pathProject As String = My.Application.Info.DirectoryPath
    Public imagePathPlayer1 As String = pathProject & "\player1\"
    Public imagePathPlayer2 As String = pathProject & "\player2\"

    Public pathbgm As String = pathProject & "\music\bgm.wav"
    Public pathsfx As String = pathProject & "\music\sfx.wav"
    Dim audiobgm As SoundPlayer = New SoundPlayer(pathbgm)
    Public multisound As New Multiplay

    Public Sub setallmusic()
        multisound.AddSound("sfx", pathsfx)
    End Sub



    Function AI_On() As Boolean
        If AI > 0 Then
            Return True
        Else
            Return False
        End If

    End Function

    Sub PlayLoopingBackgroundSoundFile()
        If bgm Then

            audiobgm.PlayLooping()
        Else
            audiobgm.Stop()
        End If
    End Sub

    Sub PlaySoundEffectOnce()
        If sfx Then
            multisound.Play("sfx")
        End If
    End Sub
    Sub hovering()
        If hvr Then
            'b
        End If
    End Sub

End Module
