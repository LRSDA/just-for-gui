﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Fsetting
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Fsetting))
        Me.SettingsLabel = New System.Windows.Forms.Label()
        Me.DifficultyLabel = New System.Windows.Forms.Label()
        Me.btEasy = New System.Windows.Forms.Button()
        Me.btMedium = New System.Windows.Forms.Button()
        Me.btHard = New System.Windows.Forms.Button()
        Me.lbPlayer = New System.Windows.Forms.Label()
        Me.tb1 = New System.Windows.Forms.TextBox()
        Me.tb2 = New System.Windows.Forms.TextBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.btCancel = New System.Windows.Forms.Button()
        Me.btOk = New System.Windows.Forms.Button()
        Me.player22 = New System.Windows.Forms.Button()
        Me.player21 = New System.Windows.Forms.Button()
        Me.player12 = New System.Windows.Forms.Button()
        Me.player11 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'SettingsLabel
        '
        Me.SettingsLabel.AutoSize = True
        Me.SettingsLabel.BackColor = System.Drawing.Color.Transparent
        Me.SettingsLabel.Font = New System.Drawing.Font("Candara", 72.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SettingsLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SettingsLabel.Location = New System.Drawing.Point(103, -21)
        Me.SettingsLabel.Name = "SettingsLabel"
        Me.SettingsLabel.Size = New System.Drawing.Size(377, 117)
        Me.SettingsLabel.TabIndex = 0
        Me.SettingsLabel.Text = "Settings"
        '
        'DifficultyLabel
        '
        Me.DifficultyLabel.AutoSize = True
        Me.DifficultyLabel.BackColor = System.Drawing.Color.Transparent
        Me.DifficultyLabel.Font = New System.Drawing.Font("Candara", 36.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DifficultyLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DifficultyLabel.Location = New System.Drawing.Point(185, 94)
        Me.DifficultyLabel.Name = "DifficultyLabel"
        Me.DifficultyLabel.Size = New System.Drawing.Size(203, 59)
        Me.DifficultyLabel.TabIndex = 1
        Me.DifficultyLabel.Text = "Difficulty"
        '
        'btEasy
        '
        Me.btEasy.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btEasy.Font = New System.Drawing.Font("Candara", 27.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btEasy.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btEasy.Location = New System.Drawing.Point(48, 159)
        Me.btEasy.Name = "btEasy"
        Me.btEasy.Size = New System.Drawing.Size(155, 59)
        Me.btEasy.TabIndex = 5
        Me.btEasy.Text = "Easy"
        Me.btEasy.UseVisualStyleBackColor = False
        '
        'btMedium
        '
        Me.btMedium.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btMedium.Font = New System.Drawing.Font("Candara", 27.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btMedium.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btMedium.Location = New System.Drawing.Point(209, 159)
        Me.btMedium.Name = "btMedium"
        Me.btMedium.Size = New System.Drawing.Size(155, 59)
        Me.btMedium.TabIndex = 6
        Me.btMedium.Text = "Medium"
        Me.btMedium.UseVisualStyleBackColor = False
        '
        'btHard
        '
        Me.btHard.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btHard.Font = New System.Drawing.Font("Candara", 27.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btHard.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btHard.Location = New System.Drawing.Point(370, 159)
        Me.btHard.Name = "btHard"
        Me.btHard.Size = New System.Drawing.Size(155, 59)
        Me.btHard.TabIndex = 7
        Me.btHard.Text = "Hard"
        Me.btHard.UseVisualStyleBackColor = False
        '
        'lbPlayer
        '
        Me.lbPlayer.AutoSize = True
        Me.lbPlayer.BackColor = System.Drawing.Color.Transparent
        Me.lbPlayer.Font = New System.Drawing.Font("Candara", 36.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbPlayer.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lbPlayer.Location = New System.Drawing.Point(212, 242)
        Me.lbPlayer.Name = "lbPlayer"
        Me.lbPlayer.Size = New System.Drawing.Size(152, 59)
        Me.lbPlayer.TabIndex = 8
        Me.lbPlayer.Text = "Player"
        '
        'tb1
        '
        Me.tb1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tb1.Font = New System.Drawing.Font("Candara", 27.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.tb1.Location = New System.Drawing.Point(94, 435)
        Me.tb1.Name = "tb1"
        Me.tb1.Size = New System.Drawing.Size(133, 53)
        Me.tb1.TabIndex = 9
        Me.tb1.Text = "player 1"
        Me.tb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tb2
        '
        Me.tb2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tb2.Font = New System.Drawing.Font("Candara", 27.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tb2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.tb2.Location = New System.Drawing.Point(347, 435)
        Me.tb2.Name = "tb2"
        Me.tb2.Size = New System.Drawing.Size(133, 53)
        Me.tb2.TabIndex = 10
        Me.tb2.Text = "player 2"
        Me.tb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.Font = New System.Drawing.Font("Candara", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CheckBox1.Location = New System.Drawing.Point(270, 531)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(255, 40)
        Me.CheckBox1.TabIndex = 11
        Me.CheckBox1.Text = "Background Music"
        Me.CheckBox1.UseVisualStyleBackColor = False
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox2.Checked = True
        Me.CheckBox2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox2.Font = New System.Drawing.Font("Candara", 21.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CheckBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CheckBox2.Location = New System.Drawing.Point(60, 531)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(178, 40)
        Me.CheckBox2.TabIndex = 12
        Me.CheckBox2.Text = "Music Effect"
        Me.CheckBox2.UseVisualStyleBackColor = False
        '
        'btCancel
        '
        Me.btCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btCancel.Font = New System.Drawing.Font("Candara", 27.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btCancel.Location = New System.Drawing.Point(142, 608)
        Me.btCancel.Name = "btCancel"
        Me.btCancel.Size = New System.Drawing.Size(139, 52)
        Me.btCancel.TabIndex = 14
        Me.btCancel.Text = "Cancel"
        Me.btCancel.UseVisualStyleBackColor = False
        '
        'btOk
        '
        Me.btOk.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btOk.Font = New System.Drawing.Font("Candara", 27.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btOk.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.btOk.Location = New System.Drawing.Point(298, 608)
        Me.btOk.Name = "btOk"
        Me.btOk.Size = New System.Drawing.Size(139, 52)
        Me.btOk.TabIndex = 15
        Me.btOk.Text = "OK"
        Me.btOk.UseVisualStyleBackColor = False
        '
        'player22
        '
        Me.player22.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.player22.BackgroundImage = CType(resources.GetObject("player22.BackgroundImage"), System.Drawing.Image)
        Me.player22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.player22.FlatAppearance.BorderSize = 0
        Me.player22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.player22.Location = New System.Drawing.Point(424, 328)
        Me.player22.Name = "player22"
        Me.player22.Size = New System.Drawing.Size(101, 101)
        Me.player22.TabIndex = 19
        Me.player22.UseVisualStyleBackColor = False
        '
        'player21
        '
        Me.player21.BackColor = System.Drawing.Color.Red
        Me.player21.BackgroundImage = CType(resources.GetObject("player21.BackgroundImage"), System.Drawing.Image)
        Me.player21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.player21.FlatAppearance.BorderSize = 0
        Me.player21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.player21.Location = New System.Drawing.Point(317, 328)
        Me.player21.Name = "player21"
        Me.player21.Size = New System.Drawing.Size(101, 101)
        Me.player21.TabIndex = 18
        Me.player21.UseVisualStyleBackColor = False
        '
        'player12
        '
        Me.player12.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.player12.BackgroundImage = CType(resources.GetObject("player12.BackgroundImage"), System.Drawing.Image)
        Me.player12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.player12.FlatAppearance.BorderSize = 0
        Me.player12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.player12.Location = New System.Drawing.Point(156, 328)
        Me.player12.Name = "player12"
        Me.player12.Size = New System.Drawing.Size(101, 101)
        Me.player12.TabIndex = 17
        Me.player12.UseVisualStyleBackColor = False
        '
        'player11
        '
        Me.player11.BackColor = System.Drawing.Color.Red
        Me.player11.BackgroundImage = CType(resources.GetObject("player11.BackgroundImage"), System.Drawing.Image)
        Me.player11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.player11.FlatAppearance.BorderSize = 0
        Me.player11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.player11.Location = New System.Drawing.Point(49, 328)
        Me.player11.Name = "player11"
        Me.player11.Size = New System.Drawing.Size(101, 101)
        Me.player11.TabIndex = 16
        Me.player11.UseVisualStyleBackColor = False
        '
        'Fsetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(580, 685)
        Me.Controls.Add(Me.player22)
        Me.Controls.Add(Me.player21)
        Me.Controls.Add(Me.player12)
        Me.Controls.Add(Me.player11)
        Me.Controls.Add(Me.btOk)
        Me.Controls.Add(Me.btCancel)
        Me.Controls.Add(Me.CheckBox2)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.tb2)
        Me.Controls.Add(Me.tb1)
        Me.Controls.Add(Me.lbPlayer)
        Me.Controls.Add(Me.btHard)
        Me.Controls.Add(Me.btMedium)
        Me.Controls.Add(Me.btEasy)
        Me.Controls.Add(Me.DifficultyLabel)
        Me.Controls.Add(Me.SettingsLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Fsetting"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "setting"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents SettingsLabel As Label
    Friend WithEvents DifficultyLabel As Label
    Friend WithEvents btEasy As Button
    Friend WithEvents btMedium As Button
    Friend WithEvents btHard As Button
    Friend WithEvents lbPlayer As Label
    Friend WithEvents tb1 As TextBox
    Friend WithEvents tb2 As TextBox
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents CheckBox2 As CheckBox
    Friend WithEvents btCancel As Button
    Friend WithEvents btOk As Button
    Friend WithEvents player11 As Button
    Friend WithEvents player12 As Button
    Friend WithEvents player21 As Button
    Friend WithEvents player22 As Button
End Class
